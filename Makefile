LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc

CC = g++
CPPFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

RM = rm -rf
RM_TUDO = rm -fr

PROG = servidor
PROG2 = cliente

.PHONY: all clean debug doc doxygen gnuplot init valgrind

all: init $(PROG) $(PROG2)

debug: CFLAGS += -g -O0
debug: $(PROG) $(PROG2)

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/

################# SERVIDOR ###################
$(PROG): $(OBJ_DIR)/servidor.o $(OBJ_DIR)/s_des.o $(OBJ_DIR)/rc4.o $(OBJ_DIR)/funcoes_chat.o $(OBJ_DIR)/diffie_hellman.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/servidor.o: $(SRC_DIR)/servidor.cpp $(INC_DIR)/s_des.h $(INC_DIR)/rc4.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

################# CLIENTE ###################
$(PROG2): $(OBJ_DIR)/cliente.o $(OBJ_DIR)/s_des.o $(OBJ_DIR)/rc4.o $(OBJ_DIR)/funcoes_chat.o $(OBJ_DIR)/diffie_hellman.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG2) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/cliente.o: $(SRC_DIR)/cliente.cpp $(INC_DIR)/s_des.h $(INC_DIR)/rc4.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

################# ARQUIVOS EM COMUM ######################
$(OBJ_DIR)/s_des.o: $(SRC_DIR)/s_des.cpp $(INC_DIR)/s_des.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/rc4.o: $(SRC_DIR)/rc4.cpp $(INC_DIR)/rc4.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/funcoes_chat.o: $(SRC_DIR)/funcoes_chat.cpp $(INC_DIR)/funcoes_chat.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/diffie_hellman.o: $(SRC_DIR)/diffie_hellman.cpp $(INC_DIR)/diffie_hellman.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes -v ./bin/multimat 2 4 8 16 32

doxygen:
	doxygen -g

doc:
	@mkdir -p $(DOC_DIR)/
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR) e $(OBJ_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
