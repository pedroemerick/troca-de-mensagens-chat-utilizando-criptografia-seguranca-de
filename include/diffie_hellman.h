/**
* @file	    diffie_hellman.cpp
* @brief	Declaração do prototipo da função que faz a troca de chaves diffie hellman
* @author   Pedro Emerick (p.emerick@live.com)
* @since    15/09/2018
* @date     15/09/2018
*/

#ifndef DIFFIE_HELLMAN_H
#define DIFFIE_HELLMAN_H

/**
* @brief    Função que realiza troca de chaves Diffie Hellman
* @param    comunicacao Com quem será feito a troca de chaves
* @return   A chave de sessão
*/
unsigned long long int diffie_hellman (int comunicacao);

#endif