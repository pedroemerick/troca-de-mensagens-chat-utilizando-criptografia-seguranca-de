/**
* @file	    rc4.h
* @brief	Declaração do prototipo da função que faz a cifragem e decifragem utilizando 
            o algoritmo Rivest Cipher 4
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/08/2018
* @date     08/09/2018
*/

#ifndef RC4_H
#define RC4_H

#include <string>
using std::string;

/**
* @brief    Função que realiza a cifragem e decifragem de mensagens utilizando o
            algoritmo Rivest Cipher 4 (RC4)
* @param    chave Chave utilizada para cifragem/decifragem
* @param    mensagem Mensagem a ser cifrada/decifrada
* @return   Mensagem cifrada/decifrada
*/
string rc4 (char *chave, string mensagem);

#endif