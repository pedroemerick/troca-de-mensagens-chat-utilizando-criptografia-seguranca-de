/**
* @file	    funcoes_chat.h
* @brief	Declaração do prototipo das funções que auxiliam o uso do chat seguro
* @author   Pedro Emerick (p.emerick@live.com)
* @since    08/08/2018
* @date     08/09/2018
*/

#ifndef FUNCOES_CHAT_H
#define FUNCOES_CHAT_H

#include <vector>
using std::vector;

#include <string>
using std::string;

/**
* @brief    Função que realiza a separação de uma string através de um delimitador
* @param    texto Texto que será separado
* @param    delimitador Delimitador utilizado
* @return   Vector com todas as palavras separadas pelo delimitador, 
            em que cada indice do vector é uma palavra
*/
vector<string> dividir_string (string texto, string delimitador);

/**
* @brief    Função que realiza a verificação das opcoes disponiveis no chat,
            se foram passadas corretamente
* @param    palavras Vector com as palavras da mensagem separadas
* @param    sair Diz se o usuário irá sair do chat
* @param    mod_cripto Diz se o usuário irá modificar a criptografia e a chave usada no chat
* @param    mod_chave Diz se o usuário irá modificar a chave usada no chat
* @param    texto Mensagem mandada/recebida no chat
* @param    enviando Diz se o usuário está enviando (true) ou recebendo (false) a mensagem
* @param    conectado Um nome para com quem está conectado ao chat (Servidor ou Cliente)
* @param    criptografia Diz a criptografia que está sendo usada no chat
*/
void opcoes_chat (vector<string> palavras, bool &sair, bool &mod_cripto, bool &mod_chave, 
                    string &texto, bool enviando, string conectado, string criptografia);

#endif