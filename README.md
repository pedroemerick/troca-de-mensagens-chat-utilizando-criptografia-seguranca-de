## CHAT COM CRIPTOGRAFIA

Este repositório contém um programa servidor e cliente, em que o servidor recebe conexões e o cliente conecta a um servidor, utilizando criptografia na trocas mensagens, aceitando os algoritmos [S-DES](https://bitbucket.org/pedroemerick/simple-des-s-des-seguranca-de-redes) e [RC4](https://bitbucket.org/pedroemerick/rivest-cipher-4-seguranca-de-redes). Para a troca de chaves é utilizado o [Diffie Hellman](https://pt.wikipedia.org/wiki/Diffie-Hellman).

---

### Considerações Gerais:

O programa permite a conexão de um servidor com um cliente, para troca de mensagens (chat) utilizando criptografia para enviar as mensagens de um lado para o outro. O chat permite o uso do algoritmo Simple-DES (SDES) e do algotimo Rivest Cipher 4 (RC4) para a criptografia. Durante o chat é possível finalizar a conexão, mudar a criptografia utilizada e também estabelecer uma nova chave com o Diffie Hellman, tudo em tempo de execução. Para o RC4, a chave de sessão é transformada em string e usada, já para o SDES, utiliza apenas os 10 bits menos significativos da chave de sessão como inteiro. Na troca de mensagens, o cliente manda uma mensagem e o servidor responde, um para um, sendo possível o envio de uma mensagem por vez, podendo enviar novamente assim que o outro lado da conexão lhe responder. O programa realiza a conexão através da porta 5354.

Sobre as opções do programa no durante a utilização do chat (todas opções são utilizadas sem as aspas):

* Para sair do chat e finalizar a conexão:
    * Utilize "--sair"
* Para trocar a criptografia utilizada:
    * Utilize "--criptografia algoritmo"
        * algoritmo = o novo metodo de criptografia que será utilizada (sdes ou rc4)
* Para trocar a chave utilizada:
    * Utilize "--chave"
        * Diffie Helman será utilizado, fornecendo uma nova chave de sessão

Sobre os comandos que o Makefile permite:

* "make clean" = apaga os arquivos objeto e binário;
* "make" = compila o programa;
* "make doc" = gera a documentação do programa, em que será possivel visualizar acesssando a pasta doc e abrindo o arquivo index.html.

---

### Para o lado servidor:
* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos:  "./servidor criptografia";
    * criptografia = algoritmo que deseja utilizar para a criptografia (sdes ou rc4)

---

### Para o lado cliente:
* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos:  "./cliente ip_servidor criptografia";
    * ip_servidor = IP do servidor que está aguardando a conexão
    * criptografia = algoritmo que deseja utilizar para a criptografia (sdes ou rc4)
* Observação: para que o lado cliente consiga conectar no servidor, o servidor já deve estar executando e aguardando a conexão de um cliente, para assim iniciar a troca de mensagens.



