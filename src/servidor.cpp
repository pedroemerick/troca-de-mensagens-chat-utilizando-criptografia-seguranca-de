/**
* @file	    servidor.cpp
* @brief	Arquivo com a função principal do programa, que representa o lado servidor do chat
* @author   Pedro Emerick (p.emerick@live.com)
* @since    03/09/2018
* @date     15/09/2018
*/

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;
using std::to_string;

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <cstring>
using std::memset;

#include <unistd.h>

#include <bitset>
using std::bitset;

#include <vector>
using std::vector;

#include <cstdlib>

#include <ctime>

#include "s_des.h"
#include "rc4.h"
#include "funcoes_chat.h"
#include "diffie_hellman.h"

/**
* @brief    Função que verifica se os argumentos passados ao programa são válidos
* @param    argc Números de argumentos passados
* @param    argv Vetor com os argumentos passados
*/
void verifica_arg (int argc, char *argv[]) 
{
    if (argc != 2) {
        cerr << "--> Argumentos invalidos !" << endl;
        cerr << "Utilize: ./bin/servidor tipo_criptografia" << endl;

        exit(1);
    }

    string opcao = argv[1];

    if (opcao != "sdes" && opcao != "rc4") {
        cerr << "--> Tipo de criptografia invalida !" << endl;
        cerr << "Utilize sdes (para Simple DES) ou rc4 (para Rivest Cipher 4)" << endl;

        exit (1);
    }
}

/**
* @brief    Função principal do programa que representa o lado servidor do chat
*/
int main (int argc, char *argv[]) 
{
    verifica_arg(argc, argv);
    
    int porta = 5354;
    char msg[1000];

    string criptografia = argv[1];
    // char *chave = argv[2];

    // Configurando Socket
    sockaddr_in servAddr;
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);   // Converte Host to Network Long
    servAddr.sin_port = htons(porta);               // Converte Host to Network Short

    // Cria socket
    int server = socket(AF_INET, SOCK_STREAM, 0);

    if (server < 0) {
        cerr << "--> Erro ao criar socket do servidor !" << endl;
        exit (1);
    }

    // associar um endereço ao socket
    int bind_st = bind(server, (sockaddr*) &servAddr, sizeof(servAddr));

    if (bind_st < 0) {
        cerr << "--> Erro ao estabelecer socket do servidor !" << endl;
        exit (1);
    }

    // Habilitando para aceitar pedidos
    listen(server, 1);
    cout << "--> Aguardando conexão !" << endl;

    // Criando soquete para conexao
    sockaddr_in clienAddr;
    socklen_t clienSize = sizeof(clienAddr);

    int cliente = accept(server, (sockaddr*) &clienAddr, &clienSize);

    if (cliente < 0) {
        cerr << "--> Erro ao conectar com o cliente !" << endl;
        exit (1);
    }

    cout << "--> Cliente conectou !" << endl << endl;

    string aux = to_string (diffie_hellman (cliente));
    char *chave = new char [aux.length() + 1];
    strcpy (chave, aux.c_str());
    
    while (1)
    {
        string texto;
        vector<string> palavras;
        bool sair = false;
        bool mod_cripto = false;
        bool mod_chave = false;

        // RECEBENDO MENSAGEM
        // Limpa mensagem
        memset(&msg, 0, sizeof(msg));
        // Recebe a mensagem
        recv(cliente, (char*)&msg, sizeof(msg), 0);

        // Criptogafria
        if (criptografia == "sdes") {
            texto = sdes (chave, msg, "decifrar");
        } else if (criptografia == "rc4") {
            texto = rc4 (chave, msg);
        }

        palavras = dividir_string(texto, " ");
        opcoes_chat (palavras, sair, mod_cripto, mod_chave, texto, false, "Cliente", criptografia);

        if (sair) {
            break;
        } else if (mod_cripto) {
            criptografia = palavras[1];
            mod_cripto = false;
        } else if (mod_chave) {
            aux = to_string (diffie_hellman (cliente));
            chave = new char [aux.length() + 1];
            strcpy (chave, aux.c_str());
            mod_chave = false;
        }

        cout << "< " << inet_ntoa(clienAddr.sin_addr) << ":TCP(" << ntohs(clienAddr.sin_port) << ") : " << texto << endl;

        // ENVIANDO MENSAGEM
        cout << "> ";
        string temp;
        getline(cin, temp);

        palavras = dividir_string(temp, " ");
        opcoes_chat (palavras, sair, mod_cripto, mod_chave, texto, true, "Cliente", criptografia);

        // Criptografia
        if (criptografia == "sdes") {
            texto = sdes(chave, temp, "cifrar");
        } else if (criptografia == "rc4") {
            texto = rc4(chave, temp);
        }

        memset(&msg, 0, sizeof(msg));
        strcpy(msg, texto.c_str());

        send(cliente, (char*)&msg, sizeof(msg), 0);

        if (sair) {
            break;
        } else if (mod_cripto) {
            criptografia = palavras[1];
            mod_cripto = false;
        } else if (mod_chave) {
            aux = to_string (diffie_hellman (cliente));
            chave = new char [aux.length() + 1];
            strcpy (chave, aux.c_str());
            mod_chave = false;
        }
    }

    close(cliente);
    close(server);

    delete[] chave;

    return 0;
}