/**
* @file	    rc4.cpp
* @brief	Implementação da função que faz a cifragem e decifragem utilizando 
            o algoritmo Rivest Cipher 4
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/08/2018
* @date     08/09/2018
*/

#include "rc4.h"

#include <string>
using std::string;

#include <bitset>
using std::bitset;

#include <cstring>

/**
* @brief    Função que realiza a cifragem e decifragem de mensagens utilizando o
            algoritmo Rivest Cipher 4 (RC4)
* @param    chave Chave utilizada para cifragem/decifragem
* @param    mensagem Mensagem a ser cifrada/decifrada
* @return   Mensagem cifrada/decifrada
*/
string rc4 (char *chave, string mensagem) 
{
    int S[256];
    char T[256];

    int keylen = strlen(chave);
    char *K = new char [keylen];
    K = chave;

    /* Inicialização */
    for (int ii = 0; ii <= 255; ii++) {
        S[ii] = ii;
        T[ii] = K[ii % keylen];
    }

    /* Permutação inicial de S */
    for (int ii = 0, jj = 0; ii <= 255; ii++) {
        jj = (jj + S[ii] + T[ii]) % 256;
        
        // Troca S[ii] e S[jj]
        int temp = S[ii];
        S[ii] = S[jj];
        S[jj] = temp;
    }

    /* Geração de fluxo */
    string saida = "";
    for (int cont = 0, ii = 0 , jj = 0; cont < (int) mensagem.size(); cont++) {

        ii = (ii + 1) % 256;
        jj = (jj + S[ii]) % 256;

        // Troca S[ii] e S[jj]
        int temp = S[ii];
        S[ii] = S[jj];
        S[jj] = temp;

        int t = (S[ii] + S[jj]) % 256;
        int k = S[t];

        bitset<8> k_bit (k);
        bitset<8> caracter_bit (mensagem[cont]);
        bitset<8> XOR = (k_bit ^= caracter_bit);

        saida += char (XOR.to_ulong()); 
    }

    return saida;
}