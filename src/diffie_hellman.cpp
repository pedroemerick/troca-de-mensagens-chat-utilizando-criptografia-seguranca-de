/**
* @file	    diffie_hellman.cpp
* @brief	Implementação da função que faz a troca de chaves diffie hellman
* @author   Pedro Emerick (p.emerick@live.com)
* @since    15/09/2018
* @date     15/09/2018
*/

#include "diffie_hellman.h"

#include <cmath>
using std::pow;
using std::fmod;

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <sys/socket.h>

#include <random>
using std::random_device;

/**
* @brief    Função que realiza troca de chaves Diffie Hellman
* @param    comunicacao Com quem será feito a troca de chaves
* @return   A chave de sessão
*/
unsigned long long int diffie_hellman (int comunicacao) 
{
    unsigned long long int q = 7;      // 17
    unsigned long long int alpha = 3;   // 23

    // Chave privada
    srand(time(NULL));  
    random_device rd;
    unsigned long long int Xa = rd() % q;

    // Chave publica 
    unsigned long long int Ya = 1;
    for (unsigned long long int ii = 1; ii <= Xa; ii++) {
        Ya *= alpha;
    } 

    Ya = Ya % q;

    // Envia chave publica do outro lado da comunicação
    send(comunicacao, (unsigned long long int*)&Ya, sizeof(Ya), 0);

    // Recebe chave publica do outro lado da comunicação
    unsigned long long int Yb;
    recv(comunicacao, (unsigned long long int*)&Yb, sizeof(Yb), 0);

    // Chave sessão
    // unsigned long int Ks = fmod (pow (Yb, Xa), q);
    unsigned long long int Ks = 1;
    for (unsigned long long int ii = 1; ii <= Xa; ii++) {
        Ks *= Yb;
    }

    Ks = Ks % q;

    return Ks;
}