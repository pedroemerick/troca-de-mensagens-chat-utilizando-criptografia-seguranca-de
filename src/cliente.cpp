/**
* @file	    cliente.cpp
* @brief	Arquivo com a função principal do programa, que representa o lado cliente do chat
* @author   Pedro Emerick (p.emerick@live.com)
* @since    03/09/2018
* @date     15/09/2018
*/

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;
using std::to_string;

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <cstring>
using std::memset;
using std::memcpy;

#include <unistd.h>
#include <netdb.h>

#include <cstdlib>
#include <ctime>

#include "s_des.h"
#include "rc4.h"
#include "funcoes_chat.h"
#include "diffie_hellman.h"

/**
* @brief    Função que verifica se os argumentos passados ao programa são válidos
* @param    argc Números de argumentos passados
* @param    argv Vetor com os argumentos passados
*/
void verifica_arg (int argc, char *argv[]) 
{
    if (argc != 3) {
        cerr << "--> Argumentos invalidos !" << endl;
        cerr << "Utilize: ./bin/cliente ip_servidor tipo_criptografia" << endl;

        exit(1);
    }

    string opcao = argv[2];

    if (opcao != "sdes" && opcao != "rc4") {
        cerr << "--> Tipo de criptografia invalida !" << endl;
        cerr << "Utilize sdes (para Simple DES) ou rc4 (para Rivest Cipher 4)" << endl;

        exit (1);
    }
}


/**
* @brief    Função principal do programa que representa o lado cliente do chat
*/
int main (int argc, char *argv[]) 
{
    verifica_arg (argc, argv);
    
    // Porta usada para o chat
    int porta = 5354;
    char msg[1000];

    char *servidorIP = argv[1];

    string criptografia = argv[2];
    // char *chave = argv[3];
    
    // Obtem e endereco ip e pesquisa o nome no dns
    struct hostent* host = gethostbyname(servidorIP);

    sockaddr_in servAddr;
    servAddr.sin_family = AF_INET;
    memcpy((char *) &servAddr.sin_addr.s_addr, host->h_addr_list[0], host->h_length);
    servAddr.sin_port = htons(porta);

    int cliente = socket(AF_INET, SOCK_STREAM, 0);

    if (cliente < 0) {
        cerr << "--> Erro ao criar socket do cliente !" << endl;
        exit (1);
    }

    int serv_st = connect(cliente, (sockaddr*) &servAddr, sizeof(servAddr));

    if (serv_st < 0) {
        cerr << "--> Erro ao conectar com o servidor !" << endl;
        exit (1);
    }

    string aux = to_string (diffie_hellman (cliente));
    char *chave = new char [aux.length() + 1];
    strcpy (chave, aux.c_str());

    while (1)
    {
        string texto;
        vector<string> palavras;
        bool sair = false;
        bool mod_cripto = false;
        bool mod_chave = false;

        // ENVIANDO MENSAGEM
        cout << "> ";
        string temp;
        getline(cin, temp);

        palavras = dividir_string(temp, " ");
        opcoes_chat (palavras, sair, mod_cripto, mod_chave, texto, true, "Servidor", criptografia);

        // Criptografia
        if (criptografia == "sdes") {
            texto = sdes(chave, temp, "cifrar");
        } else if (criptografia == "rc4") {
            texto = rc4(chave, temp);
        }

        memset(&msg, 0, sizeof(msg));
        strcpy(msg, texto.c_str());
        // Envia a mensagem
        send(cliente, (char*)&msg, sizeof(msg), 0);

        if (sair) {
            break;
        } else if (mod_cripto) {
            criptografia = palavras[1];
            mod_cripto = false;
        } else if (mod_chave) {
            aux = to_string (diffie_hellman (cliente));
            chave = new char [aux.length() + 1];
            strcpy (chave, aux.c_str());
            mod_chave = false;
        }

        // RRECEBENDO MENSAGEM
        memset(&msg, 0, sizeof(msg));
        // Recebe a mensagem
        recv(cliente, (char*)&msg, sizeof(msg), 0);

        // Criptogafria
        if (criptografia == "sdes") {
            texto = sdes (chave, msg, "decifrar");
        } else if (criptografia == "rc4") {
            texto = rc4 (chave, msg);
        }

        palavras = dividir_string(texto, " ");
        opcoes_chat (palavras, sair, mod_cripto, mod_chave, texto, false, "Servidor", criptografia);

        if (sair) {
            break;
        } else if (mod_cripto) {
            criptografia = palavras[1];
            mod_cripto = false;
        } else if (mod_chave) {
            aux = to_string (diffie_hellman (cliente));
            chave = new char [aux.length() + 1];
            strcpy (chave, aux.c_str());
            mod_chave = false;
        }

        cout << "< " << inet_ntoa(servAddr.sin_addr) << ":TCP(" << ntohs(servAddr.sin_port) << ") : " << texto << endl;
    }

    close(cliente);

    delete[] chave;

    return 0;
}