/**
* @file	    funcoes_chat.cpp
* @brief	Implementação das funções que auxiliam o uso do chat seguro
* @author   Pedro Emerick (p.emerick@live.com)
* @since    08/09/2018
* @date     15/09/2018
*/

#include "funcoes_chat.h"

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <iostream>
using std::cout;
using std::endl;

/**
* @brief    Função que realiza a separação de uma string através de um delimitador
* @param    texto Texto que será separado
* @param    delimitador Delimitador utilizado
* @return   Vector com todas as palavras separadas pelo delimitador, 
            em que cada indice do vector é uma palavra
*/
vector<string> dividir_string (string texto, string delimitador) {
    vector<string> separados;

    size_t pos = texto.find(delimitador);
    string aux;
    while (pos != string::npos) {
        aux = texto.substr(0, pos);
        separados.push_back(aux);
        texto.erase(0, pos + delimitador.length());

        pos = texto.find(delimitador);
    }
    separados.push_back(texto);

    return separados;
}

/**
* @brief    Função que realiza a verificação das opcoes disponiveis no chat,
            se foram passadas corretamente
* @param    palavras Vector com as palavras da mensagem separadas
* @param    sair Diz se o usuário irá sair do chat
* @param    mod_cripto Diz se o usuário irá modificar a criptografia e a chave usada no chat
* @param    mod_chave Diz se o usuário irá modificar a chave usada no chat
* @param    texto Mensagem mandada/recebida no chat
* @param    enviando Diz se o usuário está enviando (true) ou recebendo (false) a mensagem
* @param    conectado Um nome para com quem está conectado ao chat (Servidor ou Cliente)
* @param    criptografia Diz a criptografia que está sendo usada no chat
*/
void opcoes_chat (vector<string> palavras, bool &sair, bool &mod_cripto, bool &mod_chave, 
                    string &texto, bool enviando, string conectado, string criptografia) 
{
    if (enviando) 
    {
        if (palavras[0] == "--sair") {
            if (palavras.size() != 1) {
                cout << "--> Parametros errados ! Use --sair" << endl;
            } else {
                cout << endl << "--> Desconectou-se !" << endl;
                sair = true;
            }   
        } else if (palavras[0] == "--criptografia") {
            if (palavras.size() != 2) {
                cout << "--> Parametros errados ! Use --criptografia tipo" << endl;
            } else if (palavras[1] != "sdes" && palavras[1] != "rc4") {
                cout << "--> Tipo de criptografia desconhecido !" << endl;
            } else {
                mod_cripto = true;
            }
        } else if (palavras[0] == "--chave") {
            if (palavras.size() != 1) {
                cout << "--> Parametros errados ! Use --chave" << endl;
            } else {
                mod_chave = true;
            }
        }
    } else {
        if (palavras[0] == "--sair") {
            if (palavras.size() == 1) {
                cout << endl << "--> " << conectado << " desconectou !" << endl;
                sair = true; 
            }
        } else if (palavras[0] == "--criptografia") {
            if (palavras.size() == 2) {
                if ((palavras[1] == "sdes") || (palavras[1] == "rc4")) {
                    mod_cripto = true;
                    texto = conectado + " mudou a criptografia !";
                }
            }
        } else if (palavras[0] == "--chave") {
            if (palavras.size() == 1) {
                mod_chave = true;
                texto = conectado + " mudou a chave !";
            }
        }
    }
}